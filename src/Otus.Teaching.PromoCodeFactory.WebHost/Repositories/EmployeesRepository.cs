﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployeesRepository : IRepository<Employee>
    {

        private EmployeeDbCondext context;

        public EmployeesRepository(EmployeeDbCondext _context)
        {
            context = _context;
        }
                
        public async Task<IEnumerable<Employee>> GetAllAsync()
        {
           return  await context.Employees.ToListAsync();
        }

        public async Task<Employee> GetByIdAsync(Guid id)
        {
            return await context.Employees.SingleOrDefaultAsync(x => x.Id == id);
        }

        public async Task<bool> DeleteByIdAsync(Guid id)
        {            
            var employee = context.Employees.SingleOrDefaultAsync(x => x.Id == id);

            if (employee.Result != null)
            {
                context.Employees.Remove(employee.Result);
                await context.SaveChangesAsync();
                return true;                                            
            }
       
            return false;            
        }

        public async Task<Employee> CreateEmployeeAsync(Employee employee)
        {

            if (employee.Id == null)
            {
                employee.Id = Guid.NewGuid();
            }
           
            var existingEmployee = await GetByIdAsync(employee.Id);

            if (existingEmployee != null)
            {
                return (Employee) null; 
            }

            context.Employees.Add(employee);
            await context.SaveChangesAsync();        
            return await context.Employees.SingleOrDefaultAsync(x => x.Id == employee.Id);            
        }

        public async Task<Employee> UpdateEmployeeAsync(Employee employee)
        {
            var existingEmployee = await GetByIdAsync(employee.Id);

            if (existingEmployee == null)
            {
                return (Employee) null;
            }

            //Замена
            context.Entry(existingEmployee).CurrentValues.SetValues(employee);            ;
            await context.SaveChangesAsync();

            return await GetByIdAsync(employee.Id);

        }
    }
}
