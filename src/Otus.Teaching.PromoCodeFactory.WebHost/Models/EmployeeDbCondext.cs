﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeDbCondext: DbContext
    {
        public EmployeeDbCondext(DbContextOptions options) : base(options) { }
        
        public DbSet<Employee> Employees { get; set; }

    }
}
