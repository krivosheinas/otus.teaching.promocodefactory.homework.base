﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("[controller]")]
    public class EmployeesController
        : ControllerBase
    {

        private IRepository<Employee> repo;

        public EmployeesController (IRepository<Employee> _repo)
        {
            repo = _repo;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<Employee>> GetAllEmployeesAsync()
        {
            return await repo.GetAllAsync();
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<Employee>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await repo.GetByIdAsync(id);

            if (employee == null)
            {
                return NotFound($"Сотрудник с Id {id} не найден");
            }

            return Ok(employee);
        }

        /// <summary>
        /// Удалить сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeById(Guid id)
        {
            var res =  await repo.DeleteByIdAsync(id);            

            if (res)
            {
                return NoContent();
            }

            return NotFound($"Сотрудник с Id {id} не найден");
        }

        /// <summary>
        /// Создание нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Employee>> CreateEmployee(Employee employee)
        {
            var item = await repo.CreateEmployeeAsync(employee);

            if (item == null)
            {
                return BadRequest($"Сотрудник с Id {employee.Id} уже существует") ;
            }

            return Created($"/employees/{item.Id}", item);
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<Employee>> UpdateEmployee(Employee employee)
        {
            var item = await repo.UpdateEmployeeAsync(employee);

            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

    }
}